/*
 * a_star.cpp
 *
 * No longer needed since A* is a template class and all functionality is 
 * implemented in a_star.h
 *
 * Copyright (C) Sat Feb 11 2006 Valentin Koch & Michael Henderson
 */


#include "a_star.h"

/**
 * @param graph 
 * @param problem 
 * @param goal 
 */
